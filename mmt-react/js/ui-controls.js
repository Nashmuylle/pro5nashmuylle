function ALabel(props) {
    const style = {
        display: "inline-block",
        color: "#ffffff",
        backgroundColor: "#3d94f6",
        textShadow: "0px 1px 0px #1570cd",
        padding: "6px 24px",
        textShadow: "0px 1px 0px #1570cd",
        borderRadius: "6px",
        border: "1px solid #337fed"
    }
    return (<div>
        <label style={style}>{props.caption}</label>
    </div>);
}

function AButton(props) {
    const style = {
        boxShadow: "inset 0px 1px 0px 0px #97c4fe",
        background: "linear-gradient(to bottom, #3d94f6 5%, #1e62d0 100%)",
        backgroundColor: "#3d94f6",
        borderRadius: "6px",
        border: "1px solid #337fed",
        display: "inline-block",
        cursor: "pointer",
        color: "#ffffff",
        fontFamily: "Arial",
        fontSize: "1em",
        fontWeight: "bold",
        padding: "6px 24px",
        textShadow: "0px 1px 0px #1570cd"
    }

    return (<button onClick={props.onClick}
        style={style}>{typeof props.caption !== 'undefined'
            ? props.caption : 'button'}</button>);
}

function ImageSummary(props) {
    const style = {
        borderRadius: "6px",
        width: "400px",
        height: "200px"
    }
    return (<img style={style} alt={`afbeelding van ${props.name}`} src={props.image} />
    );
}

function ImageDetail(props) {
    const style = {
        width: "25em",
    }

    return (<img style={style} alt={`afbeelding van ${props.name}`} src={props.image} />);
}

function UnorderedList(props) {
    return (<ul>
        {props.ul.map(item => (<li key={item.key}>{item.key}</li>))}
    </ul>);
}

class LikePanel extends React.Component {
    //we declareren de eigenschap waarin we de url van MmtApi opslaan 
    host = "http://localhost:5000/MmtLike";
    //De get wordt uitgevoerd als het keyvalue attribuut gedefinieerd is. Zo blijft onze oude code nog werken
    getLikes = () => {
        const keyValue = this.props.keyValue;
        let count = 0;
        if (typeof keyValue !== 'undefined') {
            // als de callback wordt uitgevoerd, is this niet meer in de scope
            // daarom slaan we die op in een constante en geven die met de callback mee 

            const self = this;
            const url = `${this.host}/${keyValue}`;
            fetch(url)
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        likes: data.likes
                        //Hier halen we de likes op en geven ze mee aan de variabele likes die we achteraf gebruiken
                    });
                }).then(item => console.log(item));
        }
    }
    //De volgende functie gaat de likes in de databank posten
    postLikes = () => {
        let item = {
            Key: this.props.keyValue, //Het geeft de key van het huidige item mee om de likes bij het juiste item toe te voegen
            Name: 'onbelangrijk',
            Likes: 1 //Het geeft 1 like
        };

        postData(this.host, item) //We roepen de locatie van de API op en ook het item met de like en key
            .then(data => {
                this.setState({
                    likes: data.likes //Het zet de likes gelijk aan de likes van de huidige pagina
                });
            });
    }

    constructor(props) {
        // meegeven van props aan super noodzakelijk is, omdat het de constructor van React.Component in staat stelt this.props te initialiseren
        // 🔴 Kan 'this' nog niet gebruiken.
        super(props);
        // ✅ Nu kan het wel.
        this.state = {
            likes: 0
        };
        this.getLikes(); //Deze functie zal de likes gelijk zetten aan de likes van het huidige item
    }

    incrementLike = () => {
        const keyValue = this.props.keyValue; //We zetten de keyvalue gelijk aan het huidige key item
        if (typeof keyValue !== 'undefined') { //Als het keyvalue gekend is dan posten we de likes
            this.postLikes();
            //Als het niet gekend is dan verhogen we het gewoon op het scherm maar posten we niets. Zodanig blijft de legacy code werken
        } else {
            let newCount = this.state.likes + 1;
            this.setState({
                likes: newCount
            });
        }
    };

    render() {
        return (
            <div>
                <AButton caption="I like" onClick={this.incrementLike} />
                <ALabel caption={this.state.likes} />
            </div>);
    }
}


//Opzich had ik hier verschillende onderdelen kunnen/moeten gebruiken zoals bijvoorbeeld de Abutton. Dit zou dus moeten gerefactored worden zodat
//het on par is met de rest van de code en de best practices van REACT
class CommentPanel extends React.Component {
    host = "http://localhost:5000/MmtComment";

    panelStyle = {
        display: "block",
        color: "#ffffff",
        backgroundColor: "#3d94f6",
        textShadow: "0px 1px 0px #1570cd",
        padding: "6px 24px",
        textShadow: "0px 1px 0px #1570cd",
        borderRadius: "6px",
        border: "1px solid #337fed"
    }



    postComments = (data) => {
        //console.log(data)
        let item = {
            Key: this.props.keyValue,
            Name: data.fullName,
            Text: data.comment
        };

        postData(this.host, item)
    }


    constructor(props) {
        // meegeven van props aan super noodzakelijk is, omdat het de constructor van React.Component in staat stelt this.props te initialiseren
        super(props)
        this.state = {
            fullName: "",
            comment: "",
            comments: [] //We gaan telkens de huidige comment en naam opslaan in een array zodat we deze mooi kunnen tonen 
        };

    }
    //Dit kan de default negeren; bijvoorbeel POST
    handleSubmit = (event) => {
        event.preventDefault()

        const data = this.state //Dit is de data die je naar een backend zou kunnen sturen
        this.AddComment(this.state)

        //De reden waarom we direct de data veranderen bij elke keystroke is omdata dit de "single source of truth" wordt genoemd.
        //Meer informatie hierover staat in het analyse bestand
    }
    //zolang de naam telkens anders is zal hij iets anders tonen.
    //Het is een controlled component
    handleNameChange = (event) => {
        event.preventDefault()
        // console.log(event)
        // console.log(event.target.name)
        // console.log(event.target.value)
        this.state.fullName = event.target.value

    }
    handleCommentChange = (event) => {
        event.preventDefault()
        // console.log(event)
        // console.log(event.target.name)
        // console.log(event.target.value)
        this.state.comment = event.target.value

    }

    AddComment = (item) => {

        let id = this.state.comments.length //Dit is zodat we een key hebben voor de comments 
        this.state.comments.push({ key: id, name: item.fullName, text: item.comment }) //We voegen de nieuwe comment toe aan de array van comments
        const keyValue = this.props.keyValue;
        //console.log("dit is de key" + keyValue)
        if (typeof keyValue !== 'undefined') {
            this.postComments(item);
        } else {
            this.setState({
                fullName: item.fullName,//De brackets zet de keyvalue voor de keyvalue pair die hierdoor komt
                comments: this.state.comments
            })
        }
        //console.log("dit is comments" + this.state.comments)
    }

    render() {
        const { fullName } = this.state
        const { comment } = this.state
        //console.log(this.state.comments)
        return (
            <div style={this.panelStyle}>
                <h1>Comments</h1>
                <form onSubmit={this.handleSubmit}>
                    <p><input placeholder="Your Name" name="fullName" onChange={this.handleNameChange}></input></p>
                    <p><textarea placeholder="Your comment" name="comment" onChange={this.handleCommentChange}></textarea></p>
                    <p><AButton caption="Send Comment"></AButton></p>
                </form>
            </div>

        )
    }

}

class CommentList extends React.Component {

    commentStyle = {
        display: "block",
        color: "#ffffff",
        backgroundColor: "#3d94f6",
        textShadow: "0px 1px 0px #1570cd",
        padding: "6px 24px",
        textShadow: "0px 1px 0px #1570cd",
        borderRadius: "6px",
        border: "1px solid #fffff"
    }

    host = "http://localhost:5000/MmtComment";

    constructor(props) {
        super(props); // Now 'this' is initialized by calling the parent constructor.
        this.state = {
            comments: [""]
        };

    }
    //ik maak de componentDidMount Async zodat ik zeker ben dat hij steeds iets meegkrijgt in de JSON
    //The word “async” before a function means one simple thing: a function always returns a promise.
    //Other values are wrapped in a resolved promise automatically.
    //De keuze van async boven de .then methode is omdat ik het zo leesbaarder vindt en het graag eens wou uitproberen 
    async componentDidMount() {
        const keyValue = this.props.keyValue;
        const self = this;
        const url = `${this.host}/${keyValue}`;
        const response = await fetch(url); //The keyword await makes JavaScript wait until that promise settles and returns its result.
        const json = await response.json();
        this.setState({ comments: json })
        //console.log(this.state.comments)
    }
    //Dit zet ik onder een knop zodat de comments opnieuw kunnen geladen worden
    fetchComments = () => {
        const keyValue = this.props.keyValue;
        const self = this;
        const url = `${this.host}/${keyValue}`;
        fetch(url)
            .then((response) => response.json())
            .then(json => {
                this.setState({ comments: json });
            });
    }

    render() {
        return (
            <div style={this.commentStyle}>
                <AButton onClick={this.fetchComments} caption="Reload Comments" ></AButton>
                {this.state.comments !== [] &&
                    <div style={this.commentStyle}>
                        {this.state.comments.map(item => (
                            <div id={item.id} key={item.id}>
                                <h2>Naam: {item.name}</h2>
                                <p>{item.text}</p>
                            </div>)
                        )
                        }
                    </div>
                }
                {this.state.comments.length == 0 &&
                    <p>geen comments om te tonen</p>
                }
            </div>
        )

    }
}
