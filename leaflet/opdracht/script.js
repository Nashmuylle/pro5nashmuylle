// Set the map variable
const myMap = L.map('map')

// Load the basemap
const myBasemap = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
})

// Add basemap to map id
myBasemap.addTo(myMap)

// Set view of the map
myMap.setView([51.22775740994003, 4.4193981433253855], 12) //Aanpassen naar Antwerpen

//Zelf geschreven
let requestURL = 'https://opendata.arcgis.com/datasets/70bd646006d74a2985de613c24382281_5.geojson';
let request = new XMLHttpRequest();
request.open('GET', requestURL);
request.responseType = 'json';
request.send();
request.onload = function() {
    const socialsAntwerp = request.response;
    console.log(socialsAntwerp.features);
    socialsAntwerp.features.forEach(element => addPopup(element));

}

function addPopup(socialBuilding){
    L.marker([socialBuilding.geometry.coordinates[1], socialBuilding.geometry.coordinates[0]])
        .bindPopup(
            `
        <h2>${socialBuilding.properties.NAAM}</h2>
        <p><b>Type:</b> ${socialBuilding.properties.TYPE}</p>
        <p><b>Beheerder:</b> ${socialBuilding.properties.BEHEERDER}</p>
    `
        )
        .openPopup()
        .addTo(myMap)
}