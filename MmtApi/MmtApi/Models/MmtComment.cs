﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MmtApi
{
    public class MmtComment
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }

        public MmtComment() { }
        // Ik ben niet zeker of enkel key moet mee worden gegeven. Hoe kunnen we anders de text en naam ook veranderen?
        public MmtComment(string key)
        {
            this.Id = 0;
            this.Key = key;
            this.Name = "name";
            this.Text = "text";
        }
    }
}
