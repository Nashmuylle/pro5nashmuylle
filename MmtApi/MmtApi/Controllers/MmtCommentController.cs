using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MmtApi.Models;

namespace MmtApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MmtCommentController : ControllerBase
    {

        private readonly MmtContext mmtContext;

        public MmtCommentController(MmtContext context)
        {
            mmtContext = context;
        }

        [HttpGet("{key}")]
        public List<MmtComment> GetComments(string key)
        {
            //Deze geeft een lijst van comments
            //Er is hier geen foutAfhandeling voor lege strings, dit gebeurt in de react app. 
            var mmtComments = mmtContext.MmtComments.Where(a => a.Key == key).ToList();
            return mmtComments;
        }

        [HttpPost]
        public MmtComment PostComment(MmtComment item)
        {
            var mmtComment = mmtContext.MmtComments.Where(a => a.Key == item.Key).FirstOrDefault();
            
            // Ik heb de if else hier weggedaan
            // Het maakt niet uit of de key al bestaat want deze is enkel van belang bij het ophalen van de comments
                mmtContext.MmtComments.Add(item);
                mmtContext.SaveChanges();
                mmtComment = item;
            
            return mmtComment;
        }
    }
}