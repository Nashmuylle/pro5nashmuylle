function onSignIn(googleUser) {
    // Useful data for your client-side scripts:
    var profile = googleUser.getBasicProfile();
    console.log("ID: " + profile.getId()); // Don't send this directly to your server!
    console.log('Full Name: ' + profile.getName());
    console.log('Given Name: ' + profile.getGivenName());
    console.log('Family Name: ' + profile.getFamilyName());
    console.log("Image URL: " + profile.getImageUrl());
    console.log("Email: " + profile.getEmail());
    if (document.querySelector("img") == null){
        showUserProfile(profile);
    }
    else {
        console.log("already logged in");
    }



    // The ID token you need to pass to yourespr backend:
    var id_token = googleUser.getAuthResponse().id_token;
    console.log("ID Token: " + id_token);
}
function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
    deleteUserInfo();
}
function showUserProfile(profile) {

    //voor de foto
    var picture = document.createElement("img");
    picture.src = profile.getImageUrl();
    document.getElementById('profileInfo').appendChild(picture);
    //voor de id
    var googleID = document.createElement("H1");
    googleID.innerHTML = profile.getId();
    document.getElementById('profileInfo').appendChild(googleID);
    //Voor de volledige naam
    var fullName = document.createElement("H1");
    fullName.innerHTML = profile.getName();
    document.getElementById('profileInfo').appendChild(fullName);
    //Voor de email
    var email = document.createElement("H2");
    email.innerHTML = profile.getEmail();
    document.getElementById('profileInfo').appendChild(email);
}
function deleteUserInfo() {
    document.getElementById("profileInfo").innerHTML = "";

}