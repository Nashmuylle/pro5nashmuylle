
let buttonStyle = {
    boxShadow: "inset 0px 1px 0px 0px #97c4fe",
    background: "linear-gradient(to bottom, #3d94f6 5%, #1e62d0 100%)",
    backgroundColor: "#3d94f6",
    borderRadius: "6px",
    border: "1px solid #337fed",
    display: "inline-block",
    cursor: "pointer",
    color: "#ffffff",
    fontFamily: "Arial",
    fontSize: "15px",
    fontWeight: "bold",
    padding: "6px 24px",
    textShadow: "0px 1px 0px #1570cd"
}
let labelStyle = {
    display: "inline-block",
    color: "#ffffff",
    backgroundColor: "#3d94f6",
    padding: "6px 24px",
    textShadow: "0px 1px 0px #1570cd",
    borderRadius: "6px",
    border: "1px solid #337fed"
}

function AButton(caption,incrementer) {
    return(
        <button  onClick={incrementer} style={buttonStyle}>{caption}
        </button>
    )

}
function ALabel(likes) {
    return (
        <label style={labelStyle}>{likes}</label>
    )

}
class LikePanel extends React.Component {

    render() {
        return (
            <div>
                {AButton("Like",this.incrementLike)}
                {ALabel(this.state.likes)}
            </div>
        );
    }
    constructor(props) {
        super(props);
        this.state = {
            likes: 0
        };


    }
    incrementLike = () => {
        let newCount = this.state.likes + 1;
        this.setState({
            likes: newCount
        });
    };
}
class Marvel extends React.Component {
    // we gaan veel keer createElement moeten gebruiken
    // we maken een afkorting
    e = React.createElement;
    marvelCharacterStyle = {
        list: {
            color: 'green',
            display: 'flex',
            flexWrap: 'wrap'
        },
        tile: {
            color: 'white',
            backgroundColor: 'red',
            textAlign: 'center',
            width: '10em',
            fontSize: '2em',
            fontFamily: 'Arial',
            paddingTop: '0.5em'
        },
        image: {
            color: 'blue',
            width: '10em',
            paddingTop: '0.5em'
        }
    };

    MarvelCharacter(props) {
        return (<div style={this.marvelCharacterStyle.tile} key={props.name}>
            {props.name}
            <img src={props.imageUrl}
                 style={this.marvelCharacterStyle.image} alt={"foto van " + props.name} />
            {<LikePanel></LikePanel>}
        </div>);
    }

    render() {
        return (
            <div>
                <h1>{this.props.heading}</h1>
                <div style={this.marvelCharacterStyle.list}>
                    {this.props.list.map(element => this.MarvelCharacter(element))}
                </div>
            </div>
        );
    }
}


