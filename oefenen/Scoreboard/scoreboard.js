let playerOneScore = document.querySelector("#player1Score");
let playerTwoScore = document.querySelector("#player2Score");
let playingToScore = document.querySelector("#playingTo");

function addScore(player) {
    player.innerHTML ++;
}

function resetScores() {
    playerOneScore.innerHTML = "0";
    playerTwoScore.innerHTML = "0";
    playingToScore.innerHTML = document.querySelector("input").value
}

document.querySelector("#player1Button").addEventListener('click', function() {
    if(playerOneScore.innerHTML < playingToScore.innerHTML & playerTwoScore.innerHTML < playingToScore.innerHTML)
    {
        addScore(playerOneScore);
    }

});
document.querySelector("#player2Button").addEventListener('click', function (){
    if(playerOneScore.innerHTML < playingToScore.innerHTML & playerTwoScore.innerHTML < playingToScore.innerHTML)
    {
        addScore(playerTwoScore);
    }
})

document.querySelector("#resetButton").addEventListener('click', resetScores);

document.querySelector("input").addEventListener('change', function () {
    playingToScore.innerHTML = document.querySelector("input").value;
})