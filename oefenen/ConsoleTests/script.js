//Functies voor te testen in de Console.

function isEven(number){
    if(number % 2 == 1){
        return false
    }
    else{
        return true
    }
}

function factorial(number){
    if(number == 0){
        return 1;
    }
    else {
        var factorialNumber = number;
        var total = number;
        for(i = number -1; i >= 1; i--)
        {
            total *= i;
        }
        return total

    }
}

var colors = ["red", "blue", "yellow"];

colors.forEach(function(color){
    console.log(color);
});

//Functie in een functie is als volgt.
function printColor(color){
    console.log("*******");
    console.log(color);
    console.log("********");
}

colors.forEach(printColor); //We moeten zien dat we bij de functie geen haakjes zetten. Anders voert hij deze eerst uit. En krijgen we niet
//Het gewenste resultaat.
//Hij vult automatisch het argument aan van de functie met de variabele uit de array.

//Oefening, zorg dat de array omgekeerd wordt getoond.

var getallen = [1,2,3,4];
var som = 0;

function printReverse(reverseArray){
for (var i = reverseArray.length - 1; i >= 0; i--) {
    console.log(reverseArray[i]);
}
}

//Schrijf een functie die de som van de getallen van een array teruggeeft.
function sumArray(getal)
{
    som = som + getal;
    return som;
}

getallen.forEach(sumArray);
console.log(som);